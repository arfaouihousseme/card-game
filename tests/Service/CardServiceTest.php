<?php

namespace App\Tests\Service;

use App\Factory\CardFactory;
use App\Model\Card;
use App\Service\CardService;
use App\Service\CardServiceInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CardServiceTest extends KernelTestCase
{
    private CardServiceInterface $cardService;

    public function setUp(): void
    {
        self::bootKernel();

        $container = static::getContainer();

        $this->cardService = $container->get(CardService::class);
    }

    public function testGenerateEmptyRandomArrayCards(): void
    {
        $result = $this->cardService->generateRandom(0);

        $this->assertIsArray($result);
        $this->assertEquals([], $result);
    }

    public function testGenerateRandomArrayCards(): void
    {
        $expectedCount = 10;

        $result = $this->cardService->generateRandom($expectedCount);

        $this->assertIsArray($result);
        $this->assertTrue($result[array_rand($result)] instanceof  Card);
        $this->assertCount($expectedCount, $result);
    }

    public function testIsSortedCardsWithWrongOrderColorAndTrueValues(): void
    {
        $cards = [];

        $cards[] = CardFactory::create("Carreaux", 3,  "AS", 1);

        $cards[] = CardFactory::create("Pique", 2,  "1", 2);
        $cards[] = CardFactory::create("Pique", 2,  "Roi", 13);

        $cards[] = CardFactory::create("Coeur", 1,  "3", 3);
        $cards[] = CardFactory::create("Coeur", 1,  "10", 10);

        $cards[] = CardFactory::create("Trèfle", 0,  "3", 4);

        $result = $this->cardService->isSorted($cards);

        $this->assertFalse($result);
    }

    public function testIsSortedCardsWithWrongOrderColorAndWrongValues(): void
    {
        $cards = [];

        $cards[] = CardFactory::create("Carreaux", 3,  "AS", 1);

        $cards[] = CardFactory::create("Pique", 2,  "Roi", 13);
        $cards[] = CardFactory::create("Pique", 2,  "2", 2);

        $cards[] = CardFactory::create("Coeur", 1,  "3", 3);
        $cards[] = CardFactory::create("Coeur", 1,  "10", 10);

        $cards[] = CardFactory::create("Trèfle", 0,  "3", 4);

        $result = $this->cardService->isSorted($cards);

        $this->assertFalse($result);
    }

    public function testIsSortedCardsWithTrueOrderColorAndTrueValues(): void
    {
        $cards = [];

        $cards[] = CardFactory::create("Carreaux", 1,  "AS", 1);

        $cards[] = CardFactory::create("Pique", 2,  "2", 2);
        $cards[] = CardFactory::create("Pique", 2,  "Roi", 13);

        $cards[] = CardFactory::create("Coeur", 3,  "3", 3);
        $cards[] = CardFactory::create("Coeur", 3,  "10", 10);

        $cards[] = CardFactory::create("Trèfle", 4,  "3", 4);

        $result = $this->cardService->isSorted($cards);

        $this->assertTrue($result);
    }

    public function testIsSortedCardsWithSameColorAndTrueValues(): void
    {
        $cards = [];


        $cards[] = CardFactory::create("Pique", 2,  "2", 2);
        $cards[] = CardFactory::create("Pique", 2,  "Vallet", 11);
        $cards[] = CardFactory::create("Pique", 2,  "Dame", 12);
        $cards[] = CardFactory::create("Pique", 2,  "Roi", 13);

        $result = $this->cardService->isSorted($cards);

        $this->assertTrue($result);
    }

    public function testIsSortedCardsWithSameColorAndWrongValues(): void
    {
        $cards = [];


        $cards[] = CardFactory::create("Pique", 2,  "2", 2);
        $cards[] = CardFactory::create("Pique", 2,  "Dame", 12);
        $cards[] = CardFactory::create("Pique", 2,  "Vallet", 11);
        $cards[] = CardFactory::create("Pique", 2,  "Roi", 13);

        $result = $this->cardService->isSorted($cards);

        $this->assertFalse($result);
    }

    public function testSuccessSortCards(): void
    {
        $randomCards = [];

        $randomCards[] = CardFactory::create("Carreaux", 1,  "AS", 1);
        $randomCards[] = CardFactory::create("Pique", 2,  "2", 2);
        $randomCards[] = CardFactory::create("Coeur", 3,  "3", 3);
        $randomCards[] = CardFactory::create("Pique", 2,  "Roi", 13);
        $randomCards[] = CardFactory::create("Trèfle", 4,  "3", 4);
        $randomCards[] = CardFactory::create("Coeur", 3,  "10", 10);

        $expectedSortedCards[] = CardFactory::create("Carreaux", 1,  "AS", 1);
        $expectedSortedCards[] = CardFactory::create("Pique", 2,  "2", 2);
        $expectedSortedCards[] = CardFactory::create("Pique", 2,  "Roi", 13);
        $expectedSortedCards[] = CardFactory::create("Coeur", 3,  "3", 3);
        $expectedSortedCards[] = CardFactory::create("Coeur", 3,  "10", 10);
        $expectedSortedCards[] = CardFactory::create("Trèfle", 4,  "3", 4);

        $result = $this->cardService->sort($randomCards);

        $this->assertIsArray($result);
        $this->assertEquals($expectedSortedCards, $result);
    }
}
