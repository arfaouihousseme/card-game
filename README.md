# SY6 Jeu de carte test

## Local development setup

### Init project

![public/example.png](public/example.png)

First checkout the project's source locally inside your workspace directory through:

```bash
git clone git@gitlab.com:arfaouihousseme/card-game.git
cd card_game
```

```bash
make build
```
To start game 

```bash
make php
bin/console card:play
```

Unit Tests 
```bash
vendor/bin/phpunit
```