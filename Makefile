help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
## Project
build: .destroy .setup run .composer
	@echo "\033[32mBuild successfull!\033[0m"
stop: ## Project : stop
	@echo "\033[33mStopping containers ...\033[0m"
	@bash -l -c 'docker-compose -f docker-compose.yml down'
	@echo "\033[32mContainers down!\033[0m"

run: ## Project startup
	@echo "\033[33mFiring up containers ...\033[0m"
	@bash -l -c 'docker-compose -f docker-compose.yml up -d'
	@echo "\033[32mContainers running!\033[0m"
restart: stop run  ## Project restart containers

php: ## Logging to the project's php container
	@bash -l -c 'docker-compose -f docker-compose.yml exec php /bin/bash'

console: ## Using Symfony console
	@docker-compose -f docker-compose.yml exec php sh -c "bin/console $(RUN_ARGS) -vvv"
.PHONY: composer
ifeq (composer,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(subst :,\:,$(RUN_ARGS)):.ignore;@:)
endif
composer: ## Using composer
	@docker-compose -f docker-compose.yml exec php sh -c "composer $(RUN_ARGS) -vv"
.destroy:
	@echo "\033[33mRemoving containers ...\033[0m"
	@docker-compose -f docker-compose.yml rm -v --force --stop || true
	@echo "\033[32mContainers removed!\033[0m"
migration-diff: ## Generate diff migration
	@echo "\033[33mInstalling dependencies...\033[0m"
	@docker-compose -f docker-compose.yml exec php sh -c "bin/console doctrine:migrations:diff"
## Dependencies
.composer: ## Install composer dependencies
	@echo "\033[33mInstalling dependencies...\033[0m"
	@docker-compose -f docker-compose.yml exec php sh -c "composer install --optimize-autoloader --dev --no-interaction -o"
.setup:
	@echo "\033[33mBuilding containers ...\033[0m"
	@docker-compose -f docker-compose.yml build
	@echo "\033[32mContainers built!\033[0m"