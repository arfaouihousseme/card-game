<?php

namespace App\Factory;

use App\Model\Card;
use App\Model\Color;
use App\Model\Value;

class CardFactory
{
    public static function create(
        string $colorDisplayName,
        int $colorOder,
        string $valueDisplayName,
        int $valueValue
    ): Card {
        $color = new Color($colorDisplayName, $colorOder);
        $value = new Value($valueValue, $valueDisplayName);

        return new Card($value, $color);
    }
}
