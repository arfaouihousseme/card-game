<?php

namespace App\Trait;

trait TraitDisplayName
{
    public function __construct(protected string $displayName)
    {
    }

    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    public function setDisplayName(string $displayName): self
    {
        $this->displayName = $displayName;

        return $this;
    }

    public function __toString(): string
    {
        return $this->displayName;
    }
}
