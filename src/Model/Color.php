<?php

namespace App\Model;

use App\Trait\TraitDisplayName;

class Color
{
    use TraitDisplayName {
        TraitDisplayName::__construct as __TraitDisplayNameConstruct;
    }

    public function __construct(string $displayName, protected int $order)
    {
        $this->__TraitDisplayNameConstruct($displayName);
    }

    public function getOrder(): int
    {
        return $this->order;
    }

    public function setOrder(int $order): Color
    {
        $this->order = $order;

        return $this;
    }
}
