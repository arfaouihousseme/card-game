<?php

namespace App\Model;

class Card
{
    public function __construct(private Value $value, private Color $color)
    {
    }

    public function getValue(): Value
    {
        return $this->value;
    }

    public function setValue(Value $value): Card
    {
        $this->value = $value;

        return $this;
    }

    public function getColor(): Color
    {
        return $this->color;
    }

    public function setColor(Color $color): Card
    {
        $this->color = $color;

        return $this;
    }

    public function __toString(): string
    {
        return $this->value . ' ' . $this->color;
    }
}
