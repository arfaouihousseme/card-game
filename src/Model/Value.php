<?php

namespace App\Model;

use App\Trait\TraitDisplayName;

class Value
{
    use TraitDisplayName {
        TraitDisplayName::__construct as __TraitDisplayNameConstruct;
    }

    public function __construct(protected int $value, string $displayName)
    {
        $this->__TraitDisplayNameConstruct($displayName);
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function setValue(int $value): Value
    {
        $this->value = $value;

        return $this;
    }
}
