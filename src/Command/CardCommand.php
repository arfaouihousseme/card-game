<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\CardServiceInterface;

#[AsCommand(
    name: 'card:play',
    description: 'GO play card ',
)]
class CardCommand extends Command
{
    private array $orderOfColorsRequired;

    public function __construct(
        private readonly CardServiceInterface $cardService,
        private array $availableCardColors,
        string $name = null,
    ) {
        parent::__construct($name);

        usort($this->availableCardColors, function ($color_a, $color_b) {
            return $color_a['order'] - $color_b['order'];
        });

        $this->orderOfColorsRequired = array_map(
            function ($color) { return $color['display_name']; },
            $this->availableCardColors
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln("######## Début Jeu #############");
        $output->writeln("########################");

        $randomCards = $this->cardService->generateRandom();

        $output->writeln('Main non triée :');

        foreach ($randomCards as $key => $randomCard) {
            $output->writeln(sprintf("%d ==> (%s)", $key, $randomCard));
        }

        $randomExampleIndex = array_rand($randomCards);

        $output->writeln(
            sprintf(
                'Veuillez trier la main (saisir par exemple : "%d" pour le %s) :',
                $randomExampleIndex,
                $randomCards[$randomExampleIndex]
            )
        );

        $output->writeln(
            sprintf(
                'L\'order des couleurs à suivre est le suivant : [%s]',
                    implode(', ', $this->orderOfColorsRequired)
            )
        );

        $availableChoices = array_keys($randomCards);
        $userChoices = [];

        $i = 0;

        while ($i < count($randomCards)) {
            $choice = trim(fgets(STDIN));

            if (!in_array($choice, $availableChoices)) {
                $output->writeln(
                    sprintf(
                        "Choix invalide, il faut saisir une valeur dans [%s] : ",
                        implode(', ', $availableChoices)
                    )
                );
                continue;
            }

            if (in_array($choice, array_keys($userChoices))) {
                $output->writeln(sprintf("Vous avez déjà saisi %d, il faut choisir un autre: ", $choice));
                continue;
            }

            $userChoices[$choice] = $randomCards[$choice];
            $i++;

            if (!empty($userChoices) && count($randomCards) != count($userChoices)) {
                $output->writeln(
                    sprintf(
                        "Encore %d cartes à saisir, saisie en cours : [%s]",
                        count($randomCards) - count($userChoices),
                        implode(', ', $userChoices),
                    )
                );
            }
        }

        $output->writeln(sprintf("Votre tri : [%s]",  implode(', ', $userChoices)));

        $output->writeln("Analyse en cours .... ");

        sleep(3);

        if (!$this->cardService->isSorted(array_values($userChoices))) {
            $output->writeln("Oops !! vos cartes ne sont triées");

            $correction = array_map(
                function ($cardCorrection) { return (string) $cardCorrection; },
                $this->cardService->sort($randomCards)
            );

            $output->writeln("########################");
            $output->writeln("########################");
            $output->writeln(
                sprintf(
                    "Le bon réssultat est : [%s]",
                    implode(" | ", $correction),
                )
            );

            return Command::FAILURE;
        }

        $output->writeln("Félicitations !! vos cartes sont triées");

        return Command::SUCCESS;
    }
}
