<?php

namespace App\Service;

use App\Factory\CardFactory;

class CardService implements CardServiceInterface
{
    public function __construct(
        private readonly array $availableCardValues,
        private readonly array $availableCardColors,
    ) {
    }

    public function generateRandom(?int $size = 10): array
    {
        $cards = [];

        if (0 === $size) {
            return $cards;
        }

        $i = 0;
        do {
            $randomColorIndex = array_rand($this->availableCardColors);
            $randomValueIndex = array_rand($this->availableCardValues);
            $combinationAlreadyExist = in_array($randomColorIndex.$randomValueIndex, array_keys($cards));

            if (!$combinationAlreadyExist) {
                $randomColor = $this->availableCardColors[$randomColorIndex];
                $randomValue = $this->availableCardValues[$randomValueIndex];

                $cards[$randomColorIndex.$randomValueIndex] = CardFactory::create(
                    $randomColor['display_name'],
                    $randomColor['order'],
                    $randomValue['display_name'],
                    $randomValue['value'],
                );

                $i++;
            }
        } while ($i < $size);

        return array_values($cards);
    }

    public function sort(array $cards): array
    {
        usort($cards, function ($card_a, $card_b) {
            return $card_a->getColor()->getOrder() - $card_b->getColor()->getOrder();
        });

        usort($cards, function ($card_a, $card_b) {
            if ($card_a->getColor() != $card_b->getColor()) {
                return 0;
            }

            return $card_a->GetValue()->GetValue() - $card_b->GetValue()->GetValue();
        });

        return $cards;
    }

    public function isSorted(array $cards): bool
    {
        for ($i = 1; $i < count($cards); $i++) {
            $currentCard = $cards[$i];
            $previousCard = $cards[$i - 1];

            if ($currentCard->getColor() != $previousCard->getColor()) {
                if ($currentCard->getColor()->getOrder() < $previousCard->getColor()->getOrder()) {
                    return false;
                }

                continue;
            }

            if ($currentCard->getValue()->getValue() < $previousCard->getValue()->getValue()) {
                return false;
            }
        }

        return true;
    }
}
