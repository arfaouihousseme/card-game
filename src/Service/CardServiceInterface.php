<?php

namespace App\Service;

use App\Model\Card;

interface CardServiceInterface
{
    public function generateRandom(?int $size = 10): array;

    public function sort(array $cards): array;

    public function isSorted(array $cards): bool;
}
